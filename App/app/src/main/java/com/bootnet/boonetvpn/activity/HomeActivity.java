package com.bootnet.boonetvpn.activity;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.net.VpnService;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bootnet.boonetvpn.util.Stopwatch;
import com.bootnet.boonetvpn.util.TotalTraffic;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.bootnet.boonetvpn.BuildConfig;
import com.bootnet.boonetvpn.R;
import com.bootnet.boonetvpn.model.Country;
import com.bootnet.boonetvpn.model.Server;
import com.bootnet.boonetvpn.util.BitmapGenerator;
import com.bootnet.boonetvpn.util.ConnectionQuality;
import com.bootnet.boonetvpn.util.LoadData;
import com.bootnet.boonetvpn.util.PropertiesService;
import com.bootnet.boonetvpn.util.map.MapCreator;
import com.bootnet.boonetvpn.util.map.MyMarker;

import org.mapsforge.core.graphics.Bitmap;
import org.mapsforge.core.model.LatLong;
import org.mapsforge.core.model.Point;
import org.mapsforge.map.android.graphics.AndroidGraphicFactory;
import org.mapsforge.map.android.view.MapView;
import org.mapsforge.map.layer.Layers;
import org.mapsforge.map.layer.overlay.Marker;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import de.blinkt.openvpn.VpnProfile;
import de.blinkt.openvpn.core.ConfigParser;
import de.blinkt.openvpn.core.OpenVPNService;
import de.blinkt.openvpn.core.ProfileManager;
import de.blinkt.openvpn.core.VPNLaunchHelper;
import de.blinkt.openvpn.core.VpnStatus;

import com.google.android.gms.ads.MobileAds;

public class HomeActivity extends BaseActivity {

    View view;
    public static final String EXTRA_COUNTRY = "country";
    private PopupWindow popupWindow;
    private RelativeLayout homeContextRL;
    private List<Server> countryList;
    private final String COUNTRY_FILE_NAME = "countries.json";
    private List<Country> countryLatLonList = null;
    private Layers layers;
    private List<Marker> markerList;
    private Button homeBtnChooseCountrys;

    private TextView trafficInTotally;
    private TextView trafficOutTotally;
    private TextView trafficIn;
    private TextView trafficOut;

    private BroadcastReceiver br;
    private BroadcastReceiver trafficReceiver;
    public final static String BROADCAST_ACTION = "de.blinkt.openvpn.VPN_STATUS";
    private boolean statusConnection = false;

    private WaitConnectionAsync waitConnection;
    private Button serverConnect;
    private static Stopwatch stopwatch;
    private static final int START_VPN_PROFILE = 70;
    private static OpenVPNService mVPNService;
    private VpnProfile vpnProfile;
    private boolean autoConnection;
    private boolean fastConnection;
    private boolean inBackground;
    private Server autoServer;
    private boolean isBindedService = false;
    private boolean checkHomeActivityDirect = false;

    private ProgressBar progressBarRound;
    private int pStatus = 0;
    private Handler handler = new Handler();
    private  ImageView imageProgress;
    private boolean Threadfilter = false;
    private AdView mAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        homeContextRL = (RelativeLayout) findViewById(R.id.homeContextRL);
        homeBtnChooseCountrys = (Button) findViewById(R.id.homeBtnChooseCountry);
        homeBtnChooseCountrys.setVisibility(View.GONE);

        countryList = dbHelper.getUniqueCountries();

        progressBarRound = (ProgressBar) findViewById(R.id.progressBarRound);
        imageProgress = (ImageView) findViewById(R.id.imageProgress);

        trafficIn = (TextView) findViewById(R.id.serverTrafficIn);
        trafficIn.setText("0 KB");
        trafficOut = (TextView) findViewById(R.id.serverTrafficOut);
        trafficOut.setText("0 KB");

        serverConnect = (Button) findViewById(R.id.serverConnect);
        serverConnect.setVisibility(View.GONE);

        br = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                receiveStatus(context, intent);
            }
        };

        registerReceiver(br, new IntentFilter(BROADCAST_ACTION));

        trafficReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                receiveTraffic(context, intent);
            }
        };

        registerReceiver(trafficReceiver, new IntentFilter(TotalTraffic.TRAFFIC_ACTION));

        long totalServ = dbHelper.getCount();
        if (!BuildConfig.DEBUG)
            Answers.getInstance().logCustom(new CustomEvent("Total servers")
                    .putCustomAttribute("Total servers", totalServ));

        Intent intent = getIntent();
        boolean inn = intent.getBooleanExtra("CheckHomeActivityDirect", false);
        if(inn){
            initView(intent);
            checkHomeActivityDirect = true;
        }
        boolean in = intent.getBooleanExtra("getcountylistflag", false);
        if (in) {
            homeOnClick(homeBtnChooseCountrys.getRootView());
        }
        MobileAds.initialize(this, "ca-app-pub-3940256099942544/6300978111");
    }


    private void initView(Intent intent) {
        autoConnection = intent.getBooleanExtra("autoConnection", false);
        fastConnection = intent.getBooleanExtra("fastConnection", false);
        currentServer = (Server)intent.getParcelableExtra(Server.class.getCanonicalName());
        if (currentServer == null) {
            if (connectedServer != null) {
                currentServer = connectedServer;
            } else {
                onBackPressed();
                return;
            }
        }

        serverOnClicks(serverConnect.getRootView());
        if (checkStatus()) {

            serverConnect.setText(getString(R.string.server_btn_disconnect));
            ((TextView) findViewById(R.id.serverStatus)).setText(VpnStatus.getLastCleanLogMessage(getApplicationContext()));
        } else {
            serverConnect.setText(getString(R.string.server_btn_connect));
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        initView(intent);
    }

    private void receiveTraffic(Context context, Intent intent) {
        if (checkStatus()) {
            String in = "";
            String out = "";
            in = String.format(getResources().getString(R.string.traffic_in),intent.getStringExtra(TotalTraffic.DOWNLOAD_SESSION));
            out = String.format(getResources().getString(R.string.traffic_out),intent.getStringExtra(TotalTraffic.UPLOAD_SESSION));

            trafficIn.setText(in);
            trafficOut.setText(out);
        }
    }

    private void receiveStatus(Context context, Intent intent) {
        if (checkStatus()) {
            changeServerStatus(VpnStatus.ConnectionStatus.valueOf(intent.getStringExtra("status")));
        }
        if (intent.getStringExtra("detailstatus").equals("NOPROCESS")) {
            try {
                TimeUnit.SECONDS.sleep(1);
                if (!VpnStatus.isVPNActive())
                    prepareStopVPN();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private boolean checkStatus() {
        if (connectedServer != null ) {
            return VpnStatus.isVPNActive();
        }

        return false;
    }
    @Override
    protected void onResume() {
        super.onResume();

       invalidateOptionsMenu();

        if(checkHomeActivityDirect){
            inBackground = false;
            if (currentServer.getCity() == null)
                getIpInfo(currentServer);
            if (connectedServer != null && currentServer.getIp().equals(connectedServer.getIp())) {
                hideCurrentConnection = true;
                invalidateOptionsMenu();
            }
            Intent intent = new Intent(this, OpenVPNService.class);
            intent.setAction(OpenVPNService.START_SERVICE);
            isBindedService = bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
            if (checkStatus()) {
                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (!checkStatus()) {
                    connectedServer = null;
                    serverConnect.setText(getString(R.string.server_btn_connect));
                }
            } else {
                serverConnect.setText(getString(R.string.server_btn_connect));
                if (autoConnection) {
                    prepareVpn();
                }
            }
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        inBackground = true;

        if (isBindedService) {
            isBindedService = false;
            unbindService(mConnection);
        }
    }

    @Override
    protected void onDestroy() {
        unregisterReceiver(br);
        unregisterReceiver(trafficReceiver);
        if ( popupWindow != null && popupWindow.isShowing() ){
            popupWindow.dismiss();
        }
        AndroidGraphicFactory.clearResourceMemoryCache();
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (waitConnection != null) {
            waitConnection.cancel(false);
        }
        if (isTaskRoot()) {
            startActivity(new Intent(this, HomeActivity.class));
            finish();
        }
    }

    @Override
    protected boolean useHomeButton() {
        return false;
    }

    public void homeOnClick(View view) {
         sendTouchButton("homeBtnChooseCountry");
         chooseCountry();
    }

    public void serverOnClicks(View view) {
        sendTouchButton("serverConnect");
        if (checkStatus()) {
            stopVpn();
        } else {
            prepareVpn();
        }
    }

    public void serverOnClick(View view) {
        switch (view.getId()) {
            case R.id.serverConnect:
                sendTouchButton("serverConnect");
                if (checkStatus()) {
                    stopVpn();
                } else {
                    prepareVpn();
                }
                break;
            case R.id.imageProgress:
                sendTouchButton("imageProgress");
                if (checkStatus()) {
                    stopVpn();
                } else {
                    prepareVpn();
                }
                break;
        }

    }
    private void prepareVpn() {

            new Thread(new Runnable() {
                @Override
                public void run() {
                    while (Threadfilter) {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                Log.e("Thread :", String.valueOf(pStatus));
                                progressBarRound.setProgress(pStatus);
                            }
                        });
                        try {
                            Thread.sleep(10);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        pStatus++;
                        if(pStatus == 100)
                        {
                            pStatus = 1;
                        }
                    }
                }
            }).start();

            Threadfilter = true;
        if (loadVpnProfile()) {
            waitConnection = new WaitConnectionAsync();
            waitConnection.execute();
            serverConnect.setText(getString(R.string.server_btn_disconnect));
            startVpn();
        } else {
            Threadfilter = false;
            Toast.makeText(this, getString(R.string.server_error_loading_profile), Toast.LENGTH_SHORT).show();
        }
    }
    private void startVpn() {
        stopwatch = new Stopwatch();
        connectedServer = currentServer;
        hideCurrentConnection = true;
        Intent intent = VpnService.prepare(this);
        if (intent != null) {
            VpnStatus.updateStateString("USER_VPN_PERMISSION", "", R.string.state_user_vpn_permission,
                    VpnStatus.ConnectionStatus.LEVEL_WAITING_FOR_USER_INPUT);
            try {
                startActivityForResult(intent, START_VPN_PROFILE);
            } catch (ActivityNotFoundException ane) {
                VpnStatus.logError(R.string.no_vpn_support_image);
            }
        } else {
            onActivityResult(START_VPN_PROFILE, Activity.RESULT_OK, null);
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case START_VPN_PROFILE :
                    VPNLaunchHelper.startOpenVpn(vpnProfile, getBaseContext());
                    break;
                case ADBLOCK_REQUEST :
                    Log.d(IAP_TAG, "onActivityResult(" + requestCode + "," + resultCode + "," + data);

                    if (iapHelper == null) return;

                    if (iapHelper.handleActivityResult(requestCode, resultCode, data)) {
                        Log.d(IAP_TAG, "onActivityResult handled by IABUtil.");
                    }
                    break;
            }
        }
    }
    private void stopVpn() {
        prepareStopVPN();
        ProfileManager.setConntectedVpnProfileDisconnected(this);
        if (mVPNService != null && mVPNService.getManagement() != null)
            mVPNService.getManagement().stopVPN(false);

    }
    private void prepareStopVPN() {
        statusConnection = false;
        if (waitConnection != null)
            waitConnection.cancel(false);
        Threadfilter = false;
        serverConnect.setText(getString(R.string.server_btn_connect));
        connectedServer = null;
    }

    private boolean loadVpnProfile() {
        byte[] data;
        try {
            data = Base64.decode(currentServer.getConfigData(), Base64.DEFAULT);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        ConfigParser cp = new ConfigParser();
        InputStreamReader isr = new InputStreamReader(new ByteArrayInputStream(data));
        try {
            cp.parseConfig(isr);
            vpnProfile = cp.convertProfile();
            vpnProfile.mName = currentServer.getCountryLong();
            ProfileManager.getInstance(this).addProfile(vpnProfile);
        } catch (IOException | ConfigParser.ConfigParseError e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            OpenVPNService.LocalBinder binder = (OpenVPNService.LocalBinder) service;
            mVPNService = binder.getService();
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mVPNService = null;
        }

    };

    private class WaitConnectionAsync extends AsyncTask<Void, Void, Void>
    {
        @Override
        protected Void doInBackground(Void... params) {
            try {
                TimeUnit.SECONDS.sleep(PropertiesService.getAutomaticSwitchingSeconds());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (!statusConnection) {
                if (currentServer != null)
                    dbHelper.setInactive(currentServer.getIp());

                if (fastConnection) {
                    stopVpn();
                    newConnecting(getRandomServer(), true, true);
                } else if (PropertiesService.getAutomaticSwitching()){
                    if (!inBackground)
                        showAlert();
                }
            }
        }
    }

    private void changeServerStatus(VpnStatus.ConnectionStatus status) {
        switch (status) {
            case LEVEL_CONNECTED:
                statusConnection = true;
                Threadfilter = false;
                if (!inBackground) {
                    if (PropertiesService.getDownloaded() >= 104857600 && PropertiesService.getShowRating()
                            && BuildConfig.FLAVOR != "underground") {
                        PropertiesService.setShowRating(false);
                        showRating();
                    } else {
                        chooseAction();
                    }
                }

                serverConnect.setText(getString(R.string.server_btn_disconnect));
                break;
            case LEVEL_NOTCONNECTED:
                serverConnect.setText(getString(R.string.server_btn_connect));
                break;
            default:
                serverConnect.setText(getString(R.string.server_btn_disconnect));
                statusConnection = false;
                Threadfilter = true;

        }
    }

    private void chooseAction() {
        LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService(LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.pop_up_success_conected,null);

        popupWindow = new PopupWindow(
                view,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );

        popupWindow.setOutsideTouchable(false);
        popupWindow.setFocusable(true);
        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        popupWindow.showAtLocation(homeContextRL, Gravity.CENTER,0, 0);

    }
    private void showRating() {
        LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService(LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.pop_up_rating,null);

        popupWindow = new PopupWindow(
                view,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );

        popupWindow.setOutsideTouchable(false);
        popupWindow.setFocusable(true);
        popupWindow.setBackgroundDrawable(new BitmapDrawable());

        ((Button)view.findViewById(R.id.ratingBtnSure)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
                final String appPackageName = getPackageName();
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName)));
                }
            }
        });
        ((Button)view.findViewById(R.id.ratingBtnNot)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
            }
        });


        popupWindow.showAtLocation(homeContextRL, Gravity.CENTER,0, 0);

    }
    private void chooseCountry() {
        View view = initPopUp(R.layout.pop_up_choose_country, 0.6f, 0.8f, 0.8f, 0.7f);

        Log.e("popupview",String.valueOf(view));

        final List<String> countryListName = new ArrayList<String>();
        for (Server server : countryList) {
            String localeCountryName = localeCountries.get(server.getCountryShort()) != null ?
                    localeCountries.get(server.getCountryShort()) : server.getCountryLong();
            countryListName.add(localeCountryName);
        }

        ListView lvCountry = (ListView) view.findViewById(R.id.homeCountryList);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, countryListName);

        lvCountry.setAdapter(adapter);
        lvCountry.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                popupWindow.dismiss();
                onSelectCountry(countryList.get(position));
            }
        });
        homeContextRL.post(new Runnable() {
            @Override
            public void run() {
                popupWindow.showAtLocation(homeContextRL, Gravity.CENTER,0, 0);
            }
        });
    }

    private void showNote() {
        View view = initPopUp(R.layout.pop_up_note, 0.6f, 0.5f, 0.9f, 0.4f);
        ((TextView) view.findViewById(R.id.noteLink)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.vpngate.net/en/join.aspx"));
                startActivity(in);
            }
        });

        popupWindow.showAtLocation(homeContextRL, Gravity.CENTER,0, 0);

        PropertiesService.setShowNote(false);
    }

    private View initPopUp(int resourse,
                           float landPercentW,
                           float landPercentH,
                           float portraitPercentW,
                           float portraitPercentH) {

        LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService(LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(resourse, null);

        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            popupWindow = new PopupWindow(
                    view,
                    (int)(widthWindow * landPercentW),
                    (int)(heightWindow * landPercentH)
            );
        } else {
            popupWindow = new PopupWindow(
                    view,
                    (int)(widthWindow * portraitPercentW),
                    (int)(heightWindow * portraitPercentH)
            );
        }


        popupWindow.setOutsideTouchable(false);
        popupWindow.setFocusable(true);
        popupWindow.setBackgroundDrawable(new BitmapDrawable());

        return view;
    }

    private void onSelectCountry(Server server) {
        Intent intent = new Intent(getApplicationContext(), ServersListActivity.class);
        intent.putExtra(EXTRA_COUNTRY, server.getCountryShort());
        startActivity(intent);
    }
    private void showAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getString(R.string.try_another_server_text))
                .setPositiveButton(getString(R.string.try_another_server_ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                stopVpn();
                                autoServer = dbHelper.getSimilarServer(currentServer.getCountryLong(), currentServer.getIp());
                                if (autoServer != null) {
                                    newConnecting(autoServer, false, true);
                                } else {
                                    onBackPressed();
                                }
                            }
                        })
                .setNegativeButton(getString(R.string.try_another_server_no),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                if (!statusConnection) {
                                    waitConnection = new WaitConnectionAsync();
                                    waitConnection.execute();
                                }
                                dialog.cancel();
                            }
                        });
        AlertDialog alert = builder.create();
        alert.show();
    }





}

